﻿using UnityEngine;
using WarOfFractions.Core;

namespace WarOfFractions.Movement
{
    public class Mover : MonoBehaviour, IAction
    {
        #region Move
        [SerializeField] float speed = 5f;
        #endregion

        Rigidbody2D rb;
        Animator anim;
        bool canMover = true;//helper

        // Start is called before the first frame update
        void Awake()
        {
            rb = GetComponent<Rigidbody2D>();
            anim = GetComponent<Animator>();

            if (CheckAllyMask())
            {
                speed = Mathf.Abs(speed);
            }
            else
            {
                speed *= -1;//moving in opposite direction then set   
            }
        }

        public bool CheckAllyMask()
        {
            //Checking if character is onn Ally Mask and changing sign of speed according to mask
            if (gameObject.layer == 10)//ally layer
            {
                return true;
            }
            else if (gameObject.layer == 9)//enemy character layer
            {
                return false;
            }
            else
            {
                Debug.Log(gameObject.layer.ToString());
                return false;
            }
        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if(canMover)
            Movement();
        }

        private void Movement()
        {
            anim.SetBool("isMoving", true);
            Vector2 movement = new Vector2(speed * Time.deltaTime, rb.velocity.y);
            rb.velocity = movement;
        }

        public void Cancel()
        {
            canMover = false;
        }
    }
}
