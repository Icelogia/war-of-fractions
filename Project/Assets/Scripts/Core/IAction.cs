﻿using UnityEngine;

namespace WarOfFractions.Core
{
    public interface IAction
    {
        void Cancel();
    }
}
