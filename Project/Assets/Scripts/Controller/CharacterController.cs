﻿using UnityEngine;
using WarOfFractions.Movement;
using WarOfFractions.Core;
using WarOfFractions.Combat;

namespace WarOfFractions.Controller
{
    public class CharacterController : MonoBehaviour
    {
        [SerializeField] float attackTriggerDistance = 2f;

        Vector2 fightingSideDetection;

        Fighter fighter;
        Mover mover;
        Health target;
        LayerMask enemyLayer;

        private void Awake()
        {
            target = null;
            mover = GetComponent<Mover>();
            fighter = GetComponent<Fighter>();
        }
        // Start is called before the first frame update
        void Start()
        {
            if(mover.CheckAllyMask())
            {
                enemyLayer = LayerMask.GetMask("Enemy Character");
                fightingSideDetection = Vector2.right;
            }
            else
            {
                enemyLayer = LayerMask.GetMask("Ally Character");
                fightingSideDetection = Vector2.left;
            }

        }

        // Update is called once per frame
        void Update()
        {
            if(CanAttack())
            {
                fighter.Attack(target);
            }
        }
        private bool CanAttack()
        {
            //detecting enemy in the way
            RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, fightingSideDetection, attackTriggerDistance, enemyLayer);
            Health enemy = null;
            
            if (hitInfo) enemy = hitInfo.transform.GetComponent<Health>();

            if (enemy == null) return false;
            if (!hitInfo.transform.GetComponent<Health>())
            {
                return false;
            }
            else
            {
                target = enemy;
                return true;
            }
        }
    }

    
}
