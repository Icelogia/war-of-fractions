﻿using UnityEngine;

namespace WarOfFractions.Controller
{
    public class Controller : MonoBehaviour
    {
        #region Spawn
        public Transform pointOfSpawn;
        #endregion

        public void SpawnCharacter(string character)
        {
            ObjectPooler.Instance.SpawnFromPool(character, pointOfSpawn);
        }
    }
}
