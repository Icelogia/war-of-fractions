﻿using UnityEngine;

namespace WarOfFractions.Controller
{
    public class EnemyController : Controller
    {
        [SerializeField] float timeToSpawnAnEnemy = 2f;
        private float currentTime = Mathf.Infinity;

        private void Update()
        {
            if(currentTime >= timeToSpawnAnEnemy)
            {
                SpawnCharacter("Character1");
                currentTime = 0f;
            }
            currentTime += Time.deltaTime;
        }
    }
}
