﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] float movePosition = 7f;
    [SerializeField] float smoothnes = 5f;
    
    Camera mainCamera;
    Transform cameraControllerTransform;
    // Start is called before the first frame update
    void Start()
    {
        mainCamera = Camera.main;
        cameraControllerTransform = transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 mousePosition = mainCamera.ScreenToWorldPoint(Input.mousePosition);
        MovingCamera(mousePosition);
    }

    private void MovingCamera(Vector3 mousePosition)
    {
        if (mousePosition.x < cameraControllerTransform.position.x - movePosition)
        {
            cameraControllerTransform.position = cameraControllerTransform.position + Vector3.left * smoothnes * Time.deltaTime;
        }
        if (mousePosition.x > cameraControllerTransform.position.x + movePosition)
        {
            cameraControllerTransform.position = cameraControllerTransform.position + Vector3.right * smoothnes * Time.deltaTime;
        }
    }
}
