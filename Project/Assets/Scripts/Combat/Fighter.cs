﻿using UnityEngine;
using WarOfFractions.Core;
using WarOfFractions.Movement;

namespace WarOfFractions.Combat
{
    public class Fighter : MonoBehaviour, IAction
    {
        [SerializeField] int dmg = 5;
        Health target;
        Mover mover;
        // Start is called before the first frame update
        void Start()
        {
            target = null;
            mover = GetComponent<Mover>();
        }

        private void Update()
        {
            if (target == null) return;
            else
            {
                StartAttack();
            }
        }


        private void StartAttack()
        {
            mover.Cancel();
            GetComponent<Animator>().SetBool("isMoving", false);
            GetComponent<Animator>().SetBool("isAttacking", true);
        }

        private void StopAttack()
        {
            GetComponent<Animator>().SetBool("isMoving", true);
            GetComponent<Animator>().SetBool("isAttacking", false);
        }

        public void Attack(Health enemy)
        {
            if (enemy == null) return;
            target = enemy;
        }
        //function used in animation of character's attack
        private void Hit()
        {
            target.transform.GetComponent<Health>().TakeDmg(dmg);
        }
        public void Cancel()
        {
            StopAttack();
            target = null;
        }
    }
}
